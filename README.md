# Exploring Genre Relations in Song Lyrics_Topic Modeling Approach

## Overview

This GitLab repository contains code and files associated with the project on song lyric topic modeling. The project aims to analyze genres of song lyrics using various topic modeling techniques, including Latent Semantic Indexing (LSI), Non-Negative Matrix Factorization (NMF), Latent Dirichlet Allocation (LDA), and BERTopic.
Python scripts for data preprocessing, model selection, and genre-topic distribution analysis, as well as text files and visualizations of the results, can be found in the respective folders.

## Aknowledgment
This project used Kaggle’s “Song lyrics from 79 musical genres” dataset compiled by Anderson Neisse: https://www.kaggle.com/datasets/neisse/scrapped-lyrics-from-6-genres/ It comprises two primary files: "artists-data.csv" and "lyrics-data.